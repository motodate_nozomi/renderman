#include <windows.h>
#include <Core>

#include "../Window/Window.h"
#include "../Graphics/RendererBase.h"
#include "../Graphics/BasePassRenderer.h"
#include "../Common/Helper.h"


_Use_decl_annotations_
int WINAPI WinMain(HINSTANCE HInstance, HINSTANCE, LPSTR, int NCmdShow)
{

	Eigen::Vector4f TestVec {0.0f, 1.0f, 2.0f, 3.0f };

	std::wstring TestVecStr = std::to_wstring(sizeof(Eigen::RowVector4f));

	

	OutputDebugStringEx( L"%f %f %f %f \n", TestVec[0], TestVec[1], TestVec[2], TestVec[3]);

	BasePassRenderer Renderer = BasePassRenderer(1280, 760, L"RenderMan");

	return Window::Run(&Renderer, HInstance, NCmdShow);;
}