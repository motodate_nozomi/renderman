#pragma once

#include <stdexcept>
//#include <d3d12.h>
//#include <dxgi1_6.h>
//#include <D3Dcompiler.h>
//#include <DirectXMath.h>
//#include <windows.h>
#include <wrl.h>
#include <string.h>

using Microsoft::WRL::ComPtr;

inline const std::string HResultToString(const HRESULT HR)
{
    char Str[256] = {};
    sprintf_s(Str, "HRESULT of 0x%08X", static_cast<UINT>(HR));

    return std::string(Str);
}

class HResultException : public std::runtime_error
{
public:
    HResultException(HRESULT HR) 
        : std::runtime_error(HResultToString(HR))
        , HR(HR) {}

private:
    const HRESULT HR{};
};

__forceinline void ThrowIfFailed(const HRESULT HR, const std::wstring& Message = L"")
{
    if(FAILED(HR))
    {
        OutputDebugStringW(Message.c_str());
        throw HResultException(HR);
    }
}

__forceinline void GetAssetsPath(_Out_writes_(PahtSize) WCHAR* Path, UINT PahtSize)
{
    if(!Path)
    {
        throw std::exception();
    }

    DWORD ModuleFileNameSize = GetModuleFileName(nullptr, Path, PahtSize);
    if(ModuleFileNameSize <= 0 || ModuleFileNameSize == PahtSize)
    {
        throw std::exception();
    }

    WCHAR* LastSlash = wcsrchr(Path, L'\\');
    if(LastSlash)
    {
        *(LastSlash + 1) = L'\0';
    }
}

#ifdef _DEBUG
#define OutputDebugStringEx(Specifier, ...) \
    { \
        WCHAR C[256]; \
        swprintf_s(C, Specifier, __VA_ARGS__); \
        OutputDebugStringW(C); \
    }
#else
    #define OutputDebugStringEx(Specifier, ...) // Release���͋�
#endif