#include "Window.h"

#include <cstdint>

#include "../Graphics/RendererBase.h"

const std::int32_t Window::Run(RendererBase* Renderer, const HINSTANCE HInstance, const std::int32_t NCmdShow)
{
	if(!Renderer)
	{
		OutputDebugStringW(L"Window - Run(): Renderer is nullptr\n");
	}


	// コマンドラインパラメータの解析
	std::int32_t Argc = 0;
	LPWSTR* const Argv = CommandLineToArgvW(GetCommandLineW(), &Argc);

	// ウィンドウクラスの初期化
	WNDCLASSEX WindowClassEx = {0};
	WindowClassEx.cbSize = sizeof(WNDCLASSEX);
	WindowClassEx.style = CS_HREDRAW | CS_VREDRAW;
	WindowClassEx.lpfnWndProc = WindowProc;
	WindowClassEx.hInstance = HInstance;
	WindowClassEx.hCursor = LoadCursor(nullptr, IDC_ARROW);
	WindowClassEx.lpszClassName = L"RendererBase";
	RegisterClassEx(&WindowClassEx);

	LONG WindowWidth = Renderer ? static_cast<LONG>(Renderer->GetViewportWidth()) : 1280;
	LONG WindowHeight = Renderer ? static_cast<LONG>(Renderer->GetViewportHeight()) : 760;

	RECT WindowRect = {0, 0, WindowWidth, WindowHeight};
	AdjustWindowRect(&WindowRect, WS_OVERLAPPEDWINDOW, false);

	// ウィンドウ生成
	HWnd = CreateWindow(
		WindowClassEx.lpszClassName,
		Renderer != nullptr ?  Renderer->GetWindowTitle() : L"Test",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
        WindowRect.right - WindowRect.left,
        WindowRect.bottom - WindowRect.top,
		nullptr,
		nullptr,
		HInstance,
		Renderer);

	if(Renderer)
	{
		Renderer->Initialize();
	}

	if (ShowWindow(HWnd, NCmdShow))
	{
		OutputDebugStringW(L"Failed Create Window\n");
	}

	// メインループ
	MSG Msg = {};
    while (Msg.message != WM_QUIT)
    {
        if (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE))
        {
            TranslateMessage(&Msg);
            DispatchMessage(&Msg);
        }
    }

	if(Renderer)
	{
		Renderer->Finalize();
	}

	return static_cast<std::int32_t>(Msg.wParam);
}

LRESULT CALLBACK Window::WindowProc(HWND HWnd, UINT Message, WPARAM WParam, LPARAM LParam)
{
	RendererBase* Renderer = reinterpret_cast<RendererBase*>(GetWindowLongPtr(HWnd, GWLP_USERDATA));

	switch (Message)
	{
		case WM_CREATE:
		{
            LPCREATESTRUCT pCreateStruct = reinterpret_cast<LPCREATESTRUCT>(LParam);
            SetWindowLongPtr(HWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(pCreateStruct->lpCreateParams));
			return 0;
		}

		case WM_PAINT:
		{
			if(Renderer)
			{
				Renderer->Update();
				Renderer->Render();
			}

			return 0;
		}

        case WM_DESTROY:
        {
            PostQuitMessage(0);
            return 0;
        }

	}

	return DefWindowProc(HWnd, Message, WParam, LParam);
}

