#pragma once

#include "../Graphics/RendererBase.h"

#include <windows.h>
#include <cstdint>
#include <memory>



/// <summary>
/// ウィンドウの生成とアプリケーションの実行
/// </summary>
class Window
{
public:
	static const std::int32_t Run(RendererBase* Renderer, const HINSTANCE HInstance, const std::int32_t NCmdShow);
	
	static const HWND GetHwnd();

protected:
	static LRESULT CALLBACK WindowProc(HWND HWnd, UINT Message, WPARAM WParam, LPARAM LParam);

private:
	// C++17から使える
	static inline HWND HWnd;
};

__forceinline const HWND Window::GetHwnd()
{
    return HWnd;
}