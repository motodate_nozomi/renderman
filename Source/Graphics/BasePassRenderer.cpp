#include <fstream>

#include "BasePassRenderer.h"
#include "../Common/Helper.h"
#include "../Window/Window.h"

BasePassRenderer::BasePassRenderer(const std::uint32_t ViewportWidth, const std::uint32_t ViewportHeight, 
    const std::wstring AppName)
    : RendererBase(ViewportWidth, ViewportHeight, AppName) 
    , FrameIndex(0)
    , Viewport(0.0f, 0.0f, static_cast<float>(ViewportWidth), static_cast<float>(ViewportHeight))
    , ScissorRect(0, 0, static_cast<LONG>(ViewportWidth), static_cast<LONG>(ViewportHeight)){

}

void BasePassRenderer::Initialize()
{
    LoadPipline();
    LoadAsset();
}

void BasePassRenderer::Finalize()
{
    WaitForGpu();
    CloseHandle(FenceEvent);
}

void BasePassRenderer::Update()
{
    // 定数バッファを使う
    const float TRSSpeed = 0.005f;
    const float OffsetBounds = 1.25f;

    ConstantBufferData.Offset[0] += TRSSpeed;
    if(ConstantBufferData.Offset[0] > OffsetBounds)
    {
        ConstantBufferData.Offset[0] = -OffsetBounds;
    }
    memcpy(CBVDataBegin, &ConstantBufferData, sizeof(ConstantBufferData));
}

void BasePassRenderer::Render()
{
    PopulateCommandList();

    ID3D12CommandList* ppCommandList[] = {CommandList.Get()};
    if(CommandQueue)
    {
        CommandQueue->ExecuteCommandLists(_countof(ppCommandList), ppCommandList);
    }

    if(SwapChain)
    {
        SwapChain->Present(1, 0);
    }

    MoveToNextFrame();
}

void BasePassRenderer::LoadPipline()
{
    std::uint32_t DXGIFactoryFlags = 0;

#if defined(_DEBUG)
    ComPtr<ID3D12Debug> DebugController = {};
    if(SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&DebugController))))
    {
        if(DebugController)
        {
             DebugController->EnableDebugLayer();
             DXGIFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
        }
    }
#endif

    // デバイスの作成
    ComPtr<IDXGIFactory4> Factory = {};
    {
        ThrowIfFailed(CreateDXGIFactory2(DXGIFactoryFlags, IID_PPV_ARGS(&Factory)));
        if(!Factory)
        {
            OutputDebugStringW(L"BasePassRenderer: Factory is nullptr.\n");
            return;
        }

        // ソフトウェア
        if(bUseWarapDevice)
        {
            ComPtr<IDXGIAdapter> WarpAdapter = {};
            ThrowIfFailed(Factory->EnumWarpAdapter(IID_PPV_ARGS(&WarpAdapter)));

            ThrowIfFailed(D3D12CreateDevice(WarpAdapter.Get(),
                D3D_FEATURE_LEVEL_11_0,
                IID_PPV_ARGS(&Device)));

        }
        // ハードウェア
        else
        {
            ComPtr<IDXGIAdapter1> HardwareAdapter = {};
            GetHardwareAdapter(Factory.Get(), &HardwareAdapter);
        
            ThrowIfFailed(D3D12CreateDevice(HardwareAdapter.Get(),
                D3D_FEATURE_LEVEL_11_0,
                IID_PPV_ARGS(&Device)));
        }

        if(!Device)
        {
            OutputDebugStringW(L"BasePassRenderer: Dx12 Device is nullptr.\n");
            return;
        }

    }

    // コマンドキューの生成
    {
        D3D12_COMMAND_QUEUE_DESC QueueDesc = {};
        QueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        QueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;
        Device->CreateCommandQueue(&QueueDesc, IID_PPV_ARGS(&CommandQueue));
    }

    // スワップチェインの生成
    {
        DXGI_SWAP_CHAIN_DESC1 SwapChainDesc = {0};
        SwapChainDesc.BufferCount = FrameBufferCount;
        SwapChainDesc.Width = ViewportWidth;
        SwapChainDesc.Height = ViewportHeight;
        SwapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        SwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
        SwapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
        SwapChainDesc.SampleDesc.Count = 1;

        ComPtr<IDXGISwapChain1> SwapChain1 = {};
        Factory->CreateSwapChainForHwnd(
            this->CommandQueue.Get(),
            Window::GetHwnd(),
            &SwapChainDesc,
            nullptr,
            nullptr,
            &SwapChain1
        );

        // フルスクリーン無効
        ThrowIfFailed(Factory->MakeWindowAssociation(Window::GetHwnd(), DXGI_MWA_NO_ALT_ENTER));

        ThrowIfFailed(SwapChain1.As(&this->SwapChain));
        if(this->SwapChain)
        {
           FrameIndex = this->SwapChain->GetCurrentBackBufferIndex();
        }
    }

    // ディスクリプターヒープ作成
    {
        // RTV
        D3D12_DESCRIPTOR_HEAP_DESC RTVHeapDesc = {};
        RTVHeapDesc.NumDescriptors = FrameBufferCount;
        RTVHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
        RTVHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
        ThrowIfFailed(Device->CreateDescriptorHeap(&RTVHeapDesc, IID_PPV_ARGS(&RTVHeap)));

        RTVDescriptorSize = Device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);

        // CBV
        D3D12_DESCRIPTOR_HEAP_DESC CBVHeapDesc = {};
        CBVHeapDesc.NumDescriptors = 1;
        CBVHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        CBVHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        Device->CreateDescriptorHeap(&CBVHeapDesc, IID_PPV_ARGS(&CBVHeap));

        // SRV
        D3D12_DESCRIPTOR_HEAP_DESC SRVHeapDesc = {};
        SRVHeapDesc.NumDescriptors = 1;
        SRVHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
        SRVHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
        Device->CreateDescriptorHeap(&SRVHeapDesc, IID_PPV_ARGS(&SRVHeap));
    }

    // フレームリソースの作成
    {
        D3DX12_CPU_DESCRIPTOR_HANDLE RTVHandle(RTVHeap->GetCPUDescriptorHandleForHeapStart());

        for(std::uint32_t NumFrame = 0; NumFrame < FrameBufferCount; ++NumFrame)
        {
            ThrowIfFailed(this->SwapChain->GetBuffer(NumFrame, IID_PPV_ARGS(&RenderTargetList[NumFrame])));
            Device->CreateRenderTargetView(RenderTargetList[NumFrame].Get(), nullptr, RTVHandle);
            RTVHandle.Offset(1, RTVDescriptorSize);
        }
    }
   
   ThrowIfFailed(Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&CommandAllocator)));
   ThrowIfFailed(Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, IID_PPV_ARGS(&BundleAllocator)));
}

void BasePassRenderer::LoadAsset()
{
    if(!Device)
    {
        OutputDebugStringW(L"BasePassRenderer: Dx12 Device is nullptr.\n");
        return;
    }

    // ルートシグネチャ生成
    {
        /*       D3DX12_ROOT_SIGNATURE_DESC RootSignatureDesc = {};
               RootSignatureDesc.Initialize(0, nullptr, 0, nullptr, D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT);

               ComPtr<ID3DBlob> Signature = {};
               ComPtr<ID3DBlob> Error = {};
               ThrowIfFailed(D3D12SerializeRootSignature(&RootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &Signature, &Error));

               if(!Signature)
               {
                   OutputDebugStringW(L"BasePassRenderer LoadAsset(): Signature is nullptr.\n");
                   return;
               }

               Device->CreateRootSignature(0, Signature->GetBufferPointer(), Signature->GetBufferSize(),
                   IID_PPV_ARGS(&this->RootSignature));*/

        // CBV用ルートシグネチャ
        D3D12_FEATURE_DATA_ROOT_SIGNATURE FeatureData = {};
        FeatureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_1;
        if(FAILED(Device->CheckFeatureSupport(D3D12_FEATURE_ROOT_SIGNATURE, &FeatureData, sizeof(FeatureData))))
        {
            FeatureData.HighestVersion = D3D_ROOT_SIGNATURE_VERSION_1_0;
        }

        D3DX12_DESCRIPTOR_RANGE1 Ranges[1] {};
        D3DX12_ROOT_PARAMETER1 RootParameters[1] = {};
        Ranges[0].Initialize(D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0, 0, D3D12_DESCRIPTOR_RANGE_FLAG_DATA_STATIC);
        RootParameters[0].InitializeAsDescriptorTable(1, &Ranges[0], D3D12_SHADER_VISIBILITY_VERTEX);

        D3D12_ROOT_SIGNATURE_FLAGS RootSignatureFlags =
            D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
            D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS;

        D3DX12_VERSIONED_ROOT_SIGNATURE_DESC RootSignatureDesc = {};
        RootSignatureDesc.Initialize_1_1(_countof(RootParameters), RootParameters, 0, nullptr, RootSignatureFlags);

        ComPtr<ID3DBlob> Signature = {};
        ComPtr<ID3DBlob> Error = {};
        ThrowIfFailed(D3DX12SerializeVersionedRootSignature(&RootSignatureDesc, FeatureData.HighestVersion, &Signature, &Error));

        if (!Signature)
        {
            OutputDebugStringW(L"BasePassRenderer LoadAsset(): Signature is nullptr.\n");
            return;
        }

        ThrowIfFailed(Device->CreateRootSignature(0, Signature->GetBufferPointer(), Signature->GetBufferSize(),
            IID_PPV_ARGS(&this->RootSignature)), L"Failed Creating RootSignature.");
    }

    // パイプラインステート、シェーダーの作成
    {
        // シェーダ作成
        ComPtr<ID3DBlob> VertexShaderBlob = nullptr;
        ComPtr<ID3DBlob> PixelShaderBlob = nullptr;
#if defined(_DEBUG)
        std::uint32_t CompileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
        std::uint32_t CompileFlags = 0;
#endif
        ThrowIfFailed(D3DCompileFromFile(GetAssetsFullPath(L"shaders.hlsl").c_str(), nullptr, nullptr, "VSMain", "vs_5_0",
            CompileFlags, 0, &VertexShaderBlob, nullptr));

        ThrowIfFailed(D3DCompileFromFile(GetAssetsFullPath(L"shaders.hlsl").c_str(), nullptr, nullptr, "PSMain", "ps_5_0",
            CompileFlags, 0, &PixelShaderBlob, nullptr));
    
        // 頂点レイアウト作成
        D3D12_INPUT_ELEMENT_DESC InputElemnetDesc[] =
        {
            { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 },
            { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 }
        };

        // PSO作成(Pipeline State Object)
        D3D12_GRAPHICS_PIPELINE_STATE_DESC PsoDesc = {};
        PsoDesc.InputLayout = {InputElemnetDesc, _countof(InputElemnetDesc)};
        PsoDesc.pRootSignature = RootSignature.Get();
        PsoDesc.VS = D3DX12_SHADER_BYTECODE(VertexShaderBlob.Get());
        PsoDesc.PS = D3DX12_SHADER_BYTECODE(PixelShaderBlob.Get());
        PsoDesc.RasterizerState = D3DX12_RASTERIZER_DESC(D3D12_DEFAULT);
        PsoDesc.BlendState = D3DX12_BLEND_DESC(D3D12_DEFAULT);
        PsoDesc.DepthStencilState.DepthEnable = FALSE;
        PsoDesc.DepthStencilState.StencilEnable = FALSE;
        PsoDesc.SampleMask = UINT_MAX;
        PsoDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
        PsoDesc.NumRenderTargets = 1;
        PsoDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
        PsoDesc.SampleDesc.Count = 1;

        ThrowIfFailed(Device->CreateGraphicsPipelineState(&PsoDesc, IID_PPV_ARGS(&PipelineState)), L"Failed creating PSO.");
    }

    // コマンドリスト作成
    ThrowIfFailed(Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, CommandAllocator.Get(), PipelineState.Get(),
        IID_PPV_ARGS(&CommandList)), L"Failed Creating CommandList");

    ThrowIfFailed(CommandList->Close(), L"Failed that CmmandList Closing");

    // バンドル
    {
        // Device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_BUNDLE, BundleAllocator.Get(), PipelineState.Get(), IID_PPV_ARGS(&Bundle));
    }

    // 頂点データ作成
    {
        Vertex TriangleVertices[] = 
        {
            {{0.0f, 0.25f * ViewportAspectRatio, 0.5f}, {1.0f, 0.0f, 0.0f, 1.0f}},
            {{0.25f, -0.25f * ViewportAspectRatio, 0.5f}, {0.0f, 1.0f, 0.0f, 1.0f}},
            {{-0.25f, -0.25f * ViewportAspectRatio, 0.5f}, {0.0f, 0.0f, 1.0f, 1.0f}},
        };

        const UINT VertexBufferSize = sizeof(TriangleVertices);

        D3DX12_HEAP_PROPERTIES TmpResProp(D3D12_HEAP_TYPE_UPLOAD);
        D3DX12_RESOURCE_DESC TmpResDesc = D3DX12_RESOURCE_DESC::Buffer(VertexBufferSize);

        ThrowIfFailed(Device->CreateCommittedResource(
            &TmpResProp,
            D3D12_HEAP_FLAG_NONE,
            &TmpResDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(&VertexBuffer)), L"BasePasssRender: Failed creating VertexBuffer.");

        UINT8* VertexDataBegin = nullptr;
        D3DX12_RANGE ReadRange(0, 0);
        if(VertexBuffer)
        {
            ThrowIfFailed(VertexBuffer->Map(0, &ReadRange, reinterpret_cast<void**>(&VertexDataBegin)));
            memcpy(VertexDataBegin, TriangleVertices, sizeof(TriangleVertices));
            VertexBuffer->Unmap(0, nullptr);

            VertexBufferView.BufferLocation = VertexBuffer->GetGPUVirtualAddress();
            VertexBufferView.StrideInBytes = sizeof(Vertex);
            VertexBufferView.SizeInBytes = VertexBufferSize;
        }
        else
        {
            OutputDebugStringW(L"VertexBuffer is null. \n");
        }
    }

    // 定数バッファ作成
    {
        const UINT ConstantBufferSize = sizeof(SceneConstantBuffer);

        D3DX12_HEAP_PROPERTIES HeapType(D3D12_HEAP_TYPE_UPLOAD);
        D3DX12_RESOURCE_DESC RrcDesc = D3DX12_RESOURCE_DESC::Buffer(ConstantBufferSize);

        ThrowIfFailed(Device->CreateCommittedResource(
            &HeapType,
            D3D12_HEAP_FLAG_NONE,
            &RrcDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS(&ConstantBuffer)
        ), L"Failed Creating Constant Buffer");

        // ビューデスクの作成
        D3D12_CONSTANT_BUFFER_VIEW_DESC CBVDesc = {};
        CBVDesc.BufferLocation = ConstantBuffer->GetGPUVirtualAddress();
        CBVDesc.SizeInBytes = ConstantBufferSize;
        Device->CreateConstantBufferView(&CBVDesc, CBVHeap->GetCPUDescriptorHandleForHeapStart());

        D3DX12_RANGE ReadRange(0, 0);
        ThrowIfFailed(ConstantBuffer->Map(0, &ReadRange, reinterpret_cast<void**>(&CBVDataBegin)), L"Failed mapping constantBuffer");
        memcpy(CBVDataBegin, &ConstantBufferData, sizeof(ConstantBufferData));
    }

    // テクスチャ作成
    ComPtr<ID3D12Resource> TextureUploadHeap = nullptr;


    // 同期オブジェクト作成
    {
        Device->CreateFence(0, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&Fence));
        FenceValue = 1;

        FenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
        if(!FenceEvent)
        {
            ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
        }

        WaitForGpu();
    }
}

void BasePassRenderer::PopulateCommandList()
{
    if(CommandAllocator)
    {
        ThrowIfFailed(CommandAllocator->Reset(), L"PopulateCommandList: Failed Reseting CommandAllocator");
    }
   
    if(CommandList)
    {
        // コマンドリセット
        CommandList->Reset(CommandAllocator.Get(), PipelineState.Get());
        
        CommandList->SetGraphicsRootSignature(RootSignature.Get());

        // 定数バッファ設定
        ID3D12DescriptorHeap* Heaps[] = { CBVHeap.Get() };
        CommandList->SetDescriptorHeaps(_countof(Heaps), Heaps);
        CommandList->SetGraphicsRootDescriptorTable(0, CBVHeap->GetGPUDescriptorHandleForHeapStart());

        CommandList->RSSetViewports(1, &Viewport);
        CommandList->RSSetScissorRects(1, &ScissorRect);

    

        // レンダーターゲット指定
        D3DX12_RESOURCE_BARRIER TmpResourceBuffer = D3DX12_RESOURCE_BARRIER::Transition(RenderTargetList[FrameIndex].Get(),
            D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);

        CommandList->ResourceBarrier(1, &TmpResourceBuffer);

        D3DX12_CPU_DESCRIPTOR_HANDLE RTVHandle(RTVHeap->GetCPUDescriptorHandleForHeapStart(), 
            FrameIndex, RTVDescriptorSize);

        CommandList->OMSetRenderTargets(1, &RTVHandle, FALSE, nullptr);

        // コマンド記録
        const float ClearColor[] = {0.0f, 0.2f, 0.4f, 1.0f};
        CommandList->ClearRenderTargetView(RTVHandle, ClearColor, 0, nullptr);
        CommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
        CommandList->IASetVertexBuffers(0, 1, &VertexBufferView);
        CommandList->DrawInstanced(3, 1, 0, 0);

        D3DX12_RESOURCE_BARRIER TmpResBarr = D3DX12_RESOURCE_BARRIER::Transition(
            RenderTargetList[FrameIndex].Get(),
            D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);

        CommandList->ResourceBarrier(1, &TmpResBarr);

        ThrowIfFailed(CommandList->Close(), L"Failed that Command is Closing");
    }
}


void BasePassRenderer::WaitForGpu()
{
    CommandQueue->Signal(Fence.Get(), FenceValues[FrameIndex]);

    Fence->SetEventOnCompletion(FenceValues[FrameIndex], FenceEvent);
    WaitForSingleObjectEx(FenceEvent, INFINITE, FALSE);

    FenceValues[FrameIndex]++;
}

void BasePassRenderer::MoveToNextFrame()
{
    const UINT64 CurrentFenceValue = FenceValues[FrameIndex];
    ThrowIfFailed(CommandQueue->Signal(Fence.Get(), CurrentFenceValue));

    FrameIndex = SwapChain->GetCurrentBackBufferIndex();
    
    // 次のフレームの描画が終わってないならスワップできないため、待機
    if(Fence->GetCompletedValue() < FenceValues[FrameIndex])
    {
        Fence->SetEventOnCompletion(FenceValues[FrameIndex], FenceEvent);
        WaitForSingleObjectEx(FenceEvent, INFINITE, FALSE);
    }

    FenceValues[FrameIndex] = CurrentFenceValue + 1;
}

