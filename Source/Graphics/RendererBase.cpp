#include "RendererBase.h"

#include <windows.h>

#include "../Common/Helper.h"

RendererBase::RendererBase(const std::uint32_t Width, const std::uint32_t Height, const std::wstring& WindowTitle)
    : ViewportWidth(Width)
    , ViewportHeight(Height)
    , WindowTitle(WindowTitle)
{
    ViewportAspectRatio = static_cast<float>(Width) / static_cast<float>(Height);

    WCHAR AssetPath[512];
    GetAssetsPath(AssetPath, _countof(AssetPath));
    this->AssetsPath = AssetPath;
}

RendererBase::~RendererBase()
{
}

const std::wstring RendererBase::GetAssetsFullPath(LPCWSTR AssetName) const
{
    return this->AssetsPath + AssetName;
}

void RendererBase::GetHardwareAdapter(_In_ IDXGIFactory* pFactory, _Outptr_opt_result_maybenull_ IDXGIAdapter1** ppAdapter, bool RequestHighPerformanceAdapter)
{
    if(!pFactory)
    {
        OutputDebugStringW(L"pFactory is nullptr.\n");
        return;
    }

    ComPtr<IDXGIAdapter1> ResultAdapter = nullptr;
    ComPtr<IDXGIFactory6> Factory6 = nullptr;

    if(!SUCCEEDED(pFactory->QueryInterface(IID_PPV_ARGS(&Factory6))))
    {
        OutputDebugStringW(L"failed to create factory6.\n");
        return;
    }

    // アダプター(GPU)を取得できるか確認
    for(UINT AdapterIndex = 0; 
        SUCCEEDED(Factory6->EnumAdapterByGpuPreference(AdapterIndex,
            RequestHighPerformanceAdapter == true ? DXGI_GPU_PREFERENCE_HIGH_PERFORMANCE : DXGI_GPU_PREFERENCE_UNSPECIFIED,
            IID_PPV_ARGS(&ResultAdapter)));
        ++AdapterIndex)
    {
        DXGI_ADAPTER_DESC1 Desc1 {};
        ResultAdapter->GetDesc1(&Desc1);

        if(Desc1.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
        {
            continue;
        }

        if (SUCCEEDED(D3D12CreateDevice(ResultAdapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr)))
        {
            break;
        }
    }

    if(!ResultAdapter.Get())
    {
        for(UINT AdapterIndex = 0; SUCCEEDED(Factory6->EnumAdapters1(AdapterIndex, &ResultAdapter)); ++AdapterIndex)
        {
            DXGI_ADAPTER_DESC1 Desc1 {};

            if (Desc1.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
            {
                continue;
            }

            if (SUCCEEDED(D3D12CreateDevice(ResultAdapter.Get(), D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), nullptr)))
            {
                break;
            }
        }
    }

    *ppAdapter = ResultAdapter.Detach();
}
