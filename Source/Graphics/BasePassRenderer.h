#pragma once

#define EIGEN_DONT_VECTORIZE

#include <Core>
#include <Geometry>
#include <DirectXMath.h>

#include "RendererBase.h"
#include "../Graphics/D3DX12.h"

using namespace DirectX;

using Microsoft::WRL::ComPtr;

class BasePassRenderer : public RendererBase
{
public:
    BasePassRenderer(const std::uint32_t ViewportWidth, const std::uint32_t ViewportHeight, const std::wstring AppName);

    virtual void Initialize() override;
    virtual void Finalize() override;
    virtual void Update() override;
    virtual void Render() override;


private:
    // パイプラインの依存関係読み込み
    void LoadPipline();
    void LoadAsset();
    void PopulateCommandList();
    void MoveToNextFrame();
    void WaitForGpu();


    inline static const std::uint32_t FrameBufferCount = 2;
    inline static const UINT TextureWidth = 256;
    inline static const UINT TextureHeight = 256;
    inline static const UINT TexturePixelSize = 4;

    struct TestVec4
    {
        TestVec4(float x, float y, float z, float w)
        {
            elements[0] = x;
            elements[1] = y;
            elements[2] = z;
            elements[3] = w;

        }   
    
        float elements[4];

        Eigen::Vector3f t;
       
    };

    // 頂点データ
    struct alignas(32) Vertex
    {
        Eigen::Vector3f Position;
        Eigen::Vector4f Color;        

        EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    };

    // 定数バッファは256byteでアライメントの必要がある
    struct SceneConstantBuffer
    {
        Eigen::Vector4f Offset;
        //XMFLOAT4 Offset;
        float Padding[60];
    };
    static_assert((sizeof(SceneConstantBuffer) % 256) == 0, "");

    // パイプライン関連
    D3DX12_VIEWPORT Viewport = {};
    // ビュポート変換した後のどの領域を描画するか
    D3DX12_RECT ScissorRect = {};
    ComPtr<IDXGISwapChain3> SwapChain = {};
    ComPtr<ID3D12Device> Device = {};
    ComPtr<ID3D12Resource> RenderTargetList[FrameBufferCount] = {};
    ComPtr<ID3D12CommandAllocator> CommandAllocator = {};
    ComPtr<ID3D12CommandAllocator> BundleAllocator = {}; // バンドル用
    ComPtr<ID3D12CommandQueue> CommandQueue = {};
    ComPtr<ID3D12GraphicsCommandList> CommandList = {};
    ComPtr<ID3D12GraphicsCommandList> Bundle = {};
    ComPtr<ID3D12RootSignature> RootSignature = {};
    ComPtr<ID3D12DescriptorHeap> RTVHeap = {};
    ComPtr<ID3D12DescriptorHeap> CBVHeap = {};
    ComPtr<ID3D12DescriptorHeap> SRVHeap = {};
    ComPtr<ID3D12PipelineState> PipelineState = {};
    std::uint32_t RTVDescriptorSize = {};

    // リソース関連
    ComPtr<ID3D12Resource> VertexBuffer = {};
    D3D12_VERTEX_BUFFER_VIEW VertexBufferView = {};
    ComPtr<ID3D12Resource> ConstantBuffer = nullptr;
    SceneConstantBuffer ConstantBufferData = {};
    UINT8* CBVDataBegin = nullptr;
    ComPtr<ID3D12Resource> Texture = nullptr;

    // 同期オブジェクト関連
    std::uint32_t FrameIndex = {};
    HANDLE FenceEvent = {};
    ComPtr<ID3D12Fence> Fence = {};
    std::int64_t FenceValue = {};

    // 配列はstdに変更する
    UINT64 FenceValues[FrameBufferCount] = {};

};