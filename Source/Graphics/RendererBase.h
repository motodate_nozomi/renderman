#pragma once

#include <d3d12.h>
#include <dxgi1_6.h>
#include <D3Dcompiler.h>
#include <string>
#include <cstdint>
#include <wrl.h>

class RendererBase
{
public:
    RendererBase(const std::uint32_t Width, const std::uint32_t Height, const std::wstring& WindowTitle);

    virtual ~RendererBase();

    virtual void Initialize() = 0;
    virtual void Finalize() = 0;
    virtual void Update()  = 0;
    virtual void Render()  = 0;

    // ビューポート関連
    const  std::uint32_t GetViewportWidth() const;
    const  std::uint32_t GetViewportHeight() const;

    const WCHAR* GetWindowTitle() const;

    const std::wstring GetAssetsFullPath(LPCWSTR AssetName) const;

protected:
    // DX12をサポートしているハードウェアアダプターを探す(デバイスは作らない)
    void GetHardwareAdapter(_In_ IDXGIFactory* pFactory, _Outptr_opt_result_maybenull_ IDXGIAdapter1** ppAdapter,
        bool RequestHighPerformanceAdapter = false);

    // ビューポート関連
    std::uint32_t ViewportWidth = {};
    std::uint32_t ViewportHeight = {};
    float ViewportAspectRatio = {};

    // アダプター
    bool bUseWarapDevice = false;

private:
    std::wstring WindowTitle = {};
    std::wstring AssetsPath = {};
};

__forceinline const std::uint32_t RendererBase::GetViewportWidth() const
{
    return ViewportWidth;
}

__forceinline const std::uint32_t RendererBase::GetViewportHeight() const
{
    return ViewportHeight;
}

__forceinline const WCHAR* RendererBase::GetWindowTitle() const
{
    return WindowTitle.c_str();
}