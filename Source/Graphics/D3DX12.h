#pragma once

#include "d3d12.h"
#include "../Common/Helper.h"

// d3d12.hを使いやすくラップしてみた

struct D3DX12_DEFAULT {};
extern const _declspec(selectany) D3DX12_DEFAULT D3D12_DEFAULT;

/// <summary>
/// 矩形データ
/// </summary>
struct D3DX12_RECT : public D3D12_RECT
{
    D3DX12_RECT() = default;

    explicit D3DX12_RECT(const D3D12_RECT& Rect) noexcept:
        D3D12_RECT(Rect){}

    explicit D3DX12_RECT(
        LONG Left,
        LONG Top,
        LONG Right,
        LONG Bottom
    ) noexcept
    {
        left = Left;
        top = Top;
        right = Right;
        bottom = Bottom;
    }
};

/// <summary>
/// ビューポート
/// </summary>
inline bool operator==(const D3D12_VIEWPORT& Lhs, const D3D12_VIEWPORT& Rhs) noexcept
{
    return Lhs.TopLeftX == Rhs.TopLeftX && Lhs.TopLeftY == Rhs.TopLeftY && Lhs.Width == Rhs.Width &&
        Lhs.Height == Rhs.Height && Lhs.MinDepth == Rhs.MinDepth && Lhs.MaxDepth == Rhs.MaxDepth;
}

inline bool operator!=(const D3D12_VIEWPORT& Lhs, const D3D12_VIEWPORT& Rhs) noexcept
{
    return !(Lhs == Rhs);
}

struct D3DX12_VIEWPORT : public D3D12_VIEWPORT
{
    D3DX12_VIEWPORT() = default;

    explicit D3DX12_VIEWPORT(const D3D12_VIEWPORT& Viewport) noexcept: 
        D3D12_VIEWPORT(Viewport) {}

    explicit D3DX12_VIEWPORT(
        FLOAT TopLeftX,
        FLOAT TopLeftY,
        FLOAT Width,
        FLOAT Height,
        FLOAT MinDepth = D3D12_MIN_DEPTH,
        FLOAT MaxDepth = D3D12_MAX_DEPTH
    ) noexcept
    {
        this->TopLeftX = TopLeftX;
        this->TopLeftY = TopLeftY;
        this->Width = Width;
        this->Height = Height;
        this->MinDepth = MinDepth;
        this->MaxDepth = MaxDepth;
    }

    explicit D3DX12_VIEWPORT(
        _In_ ID3D12Resource* pResource,
        UINT MipSlice = 0,
        FLOAT TopLeftX = 0.0f,
        FLOAT TopLeftY = 0.0f,
        FLOAT MinDepth = D3D12_MIN_DEPTH,
        FLOAT MaxDepth = D3D12_MAX_DEPTH
    )
    {
        if (!pResource)
        {
            OutputDebugStringW(L"D3DX12_VIEWPORT: ID3D12Resource is nullptr");
            return;
        }

        const D3D12_RESOURCE_DESC& Desc = pResource->GetDesc();
        const UINT64 SubresourceWidth = Desc.Width >> MipSlice;
        const UINT64 SubresourceHeight = Desc.Height >> MipSlice;

        switch (Desc.Dimension)
        {
            case D3D12_RESOURCE_DIMENSION_BUFFER:
            {
                this->TopLeftX = TopLeftX;
                this->TopLeftY = 0.0f;
                this->Width = static_cast<float>(Desc.Width) - TopLeftX;
                this->Height = 1.0f;
                break;
            }

            case D3D12_RESOURCE_DIMENSION_TEXTURE1D:
            {
                this->TopLeftX = TopLeftX;
                this->TopLeftY = 0.0;
                this->Width = (SubresourceWidth ? static_cast<float>(SubresourceWidth) : 1.0f) - TopLeftX;
                break;
            }

            case D3D12_RESOURCE_DIMENSION_TEXTURE2D:
            case D3D12_RESOURCE_DIMENSION_TEXTURE3D:
            {
                this->TopLeftX = TopLeftX;
                this->TopLeftY = TopLeftY;
                this->Width = (SubresourceWidth ? static_cast<float>(SubresourceWidth) : 1.0f) - TopLeftX;
                this->Width = (SubresourceHeight ? static_cast<float>(SubresourceHeight) : 1.0f) - TopLeftY;
                break;
            }

            default:
            {
                OutputDebugStringW(L"D3DX12_VIEWPORT: Desc.Dimension can't use");
                break;
            }
        }

        this->MinDepth = MinDepth;
        this->MaxDepth = MaxDepth;

    }
};

// ディスクリプタヒープ
struct D3DX12_CPU_DESCRIPTOR_HANDLE : public D3D12_CPU_DESCRIPTOR_HANDLE
{
    D3DX12_CPU_DESCRIPTOR_HANDLE() = default;

    D3DX12_CPU_DESCRIPTOR_HANDLE(const D3D12_CPU_DESCRIPTOR_HANDLE &CpuDescHandle) noexcept :
        D3D12_CPU_DESCRIPTOR_HANDLE(CpuDescHandle) {}

    D3DX12_CPU_DESCRIPTOR_HANDLE(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE& OtherHandle,
        const std::int32_t OffsetInDescriptors,
        const std::int32_t OffsetScaledByIncrementsSize) noexcept
    {
        InitOffsetted(OtherHandle, OffsetInDescriptors, OffsetScaledByIncrementsSize);
    }
    
    D3DX12_CPU_DESCRIPTOR_HANDLE& Offset(const std::int32_t OffsetScaleByIncrementSize) noexcept
    {
        ptr = SIZE_T(INT64(ptr) + INT64(OffsetScaleByIncrementSize));
        return *this;
    }

    D3DX12_CPU_DESCRIPTOR_HANDLE& Offset(const std::int32_t OffsetInDescriptorList, const std::int32_t OffsetScaleByIncrementSize) noexcept
    {
        ptr = SIZE_T(INT64(ptr) + INT64(OffsetInDescriptorList) * INT64(OffsetScaleByIncrementSize));
        return *this;
    }

    __forceinline void InitOffsetted(_In_ const D3D12_CPU_DESCRIPTOR_HANDLE& BaseHandle, 
        std::int32_t OffsetInDescriptors, std::uint32_t DescriptorIncrementSize) noexcept
    {
        InitOffsetted(*this, BaseHandle, OffsetInDescriptors, DescriptorIncrementSize);
    }

    static __forceinline void InitOffsetted(_Out_ D3D12_CPU_DESCRIPTOR_HANDLE& Handle, _In_ const D3D12_CPU_DESCRIPTOR_HANDLE& BaseHandle,
        const std::int32_t OffsetInDescriptors, const std::int32_t DescriptorIncrementSize) noexcept
    {
        Handle.ptr = SIZE_T(INT64(BaseHandle.ptr) + INT64(OffsetInDescriptors) * INT64(DescriptorIncrementSize));
    }
};

// ルートシグネチャ
struct D3DX12_ROOT_SIGNATURE_DESC : public D3D12_ROOT_SIGNATURE_DESC
{
    D3DX12_ROOT_SIGNATURE_DESC() = default;

    D3DX12_ROOT_SIGNATURE_DESC(
        std::uint32_t NumParameters,
        _In_reads_opt_(NumParameters) const D3D12_ROOT_PARAMETER* _pParameters,
        std::uint32_t NumStaticSamplers = 0,
        _In_reads_opt_(NumStaticSamplers) const D3D12_STATIC_SAMPLER_DESC* _pStaticSamplers = nullptr,
        D3D12_ROOT_SIGNATURE_FLAGS Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE) noexcept
    {
        Initialize(NumParameters, _pParameters, NumStaticSamplers, _pStaticSamplers, Flags);
    }

    __forceinline void Initialize(
        std::uint32_t NumParameters,
        _In_reads_opt_(NumParameters) const D3D12_ROOT_PARAMETER* _pParameters,
        std::uint32_t NumStaticSamplers = 0,
        _In_reads_opt_(NumStaticSamplers) const D3D12_STATIC_SAMPLER_DESC* _pStaticSamplers = nullptr,
        D3D12_ROOT_SIGNATURE_FLAGS Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE)
    {
        Initialize(*this, NumParameters, _pParameters, NumStaticSamplers, _pStaticSamplers, Flags);
    }

    __forceinline void Initialize(
        _Out_ D3D12_ROOT_SIGNATURE_DESC &Desc,
        std::uint32_t NumParameters,
        _In_reads_opt_(NumParameters) const D3D12_ROOT_PARAMETER* _pParameters,
        std::uint32_t NumStaticSamplers = 0,
        _In_reads_opt_(NumStaticSamplers) const D3D12_STATIC_SAMPLER_DESC* _pStaticSamplers = nullptr,
        D3D12_ROOT_SIGNATURE_FLAGS Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE)
    {
        Desc.NumParameters = NumParameters;
        Desc.pParameters = _pParameters;
        Desc.NumStaticSamplers = NumStaticSamplers;
        Desc.pStaticSamplers = pStaticSamplers;
        Desc.Flags = Flags;
    }
};

struct D3DX12_DESCRIPTOR_RANGE1 : public D3D12_DESCRIPTOR_RANGE1
{
    D3DX12_DESCRIPTOR_RANGE1() = default;

    explicit D3DX12_DESCRIPTOR_RANGE1(const D3D12_DESCRIPTOR_RANGE1& o) noexcept :
        D3D12_DESCRIPTOR_RANGE1(o)
    {}

    inline void Initialize(
        D3D12_DESCRIPTOR_RANGE_TYPE RangeType,
        UINT NumDescriptors,
        UINT BaseShaderRegister,
        UINT RegisterSpace = 0,
        D3D12_DESCRIPTOR_RANGE_FLAGS Flags = D3D12_DESCRIPTOR_RANGE_FLAG_NONE,
        UINT OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND) noexcept
    {
        Initialize(*this, RangeType, NumDescriptors, BaseShaderRegister,
            RegisterSpace, Flags, OffsetInDescriptorsFromTableStart);
    }

    static inline void Initialize(
        _Out_ D3D12_DESCRIPTOR_RANGE1& Range,
        D3D12_DESCRIPTOR_RANGE_TYPE RangeType,
        UINT NumDescriptors,
        UINT BaseShaderRegister,
        UINT RegisterSpace = 0,
        D3D12_DESCRIPTOR_RANGE_FLAGS Flags = D3D12_DESCRIPTOR_RANGE_FLAG_NONE,
        UINT OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND) noexcept
    {
        Range.RangeType = RangeType;
        Range.NumDescriptors = NumDescriptors;
        Range.BaseShaderRegister = BaseShaderRegister;
        Range.RegisterSpace = RegisterSpace;
        Range.Flags = Flags;
        Range.OffsetInDescriptorsFromTableStart = OffsetInDescriptorsFromTableStart;
    }
};

struct D3DX12_ROOT_DESCRIPTOR_TABLE1 : public D3D12_ROOT_DESCRIPTOR_TABLE1
{
    D3DX12_ROOT_DESCRIPTOR_TABLE1() = default;

    explicit D3DX12_ROOT_DESCRIPTOR_TABLE1(const D3D12_ROOT_DESCRIPTOR_TABLE1& o) noexcept
        : D3D12_ROOT_DESCRIPTOR_TABLE1(o)
    {}

    D3DX12_ROOT_DESCRIPTOR_TABLE1(
        UINT NumDescRanges,
        _In_reads_opt_(NumDescRanges) const D3D12_DESCRIPTOR_RANGE1* DescRanges)
    {
        Initialize(NumDescRanges, DescRanges);
    }

    inline void Initialize(
        UINT NumDescRanges,
        _In_reads_opt_(NumDescRanges) const D3D12_DESCRIPTOR_RANGE1* DescRanges) noexcept
    {
        Initialize(*this, NumDescRanges, DescRanges);
    }

    static inline void Initialize(
        _Out_ D3D12_ROOT_DESCRIPTOR_TABLE1& RootDescTable,
        UINT NumDescRanges,
        _In_reads_opt_(NumDescRanges) const D3D12_DESCRIPTOR_RANGE1* DescRanges
    ) noexcept
    {
        RootDescTable.NumDescriptorRanges = NumDescRanges;
        RootDescTable.pDescriptorRanges = DescRanges;
    }
};

struct D3DX12_ROOT_PARAMETER1 : public D3D12_ROOT_PARAMETER1
{
    D3DX12_ROOT_PARAMETER1() = default;

    explicit D3DX12_ROOT_PARAMETER1(const D3D12_ROOT_PARAMETER1& o)
        : D3D12_ROOT_PARAMETER1(o)
    {}

    inline void InitializeAsDescriptorTable(
        UINT NumDescriptorRanges,
        _In_reads_(NumDescriptorRanges) const D3D12_DESCRIPTOR_RANGE1* DescriptorRanges,
        D3D12_SHADER_VISIBILITY Visibility = D3D12_SHADER_VISIBILITY_ALL
    ) noexcept
    {
        InitializeAsDescriptorTable(*this, NumDescriptorRanges, DescriptorRanges, Visibility);
    }

    static inline void InitializeAsDescriptorTable(
        _Out_ D3D12_ROOT_PARAMETER1& RootParm,
        UINT NumDescriptorRanges,
        _In_reads_(NumDescriptorRanges) const D3D12_DESCRIPTOR_RANGE1* DescriptorRanges,
        D3D12_SHADER_VISIBILITY Visibility = D3D12_SHADER_VISIBILITY_ALL) noexcept
    {
        RootParm.ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
        RootParm.ShaderVisibility = Visibility;
       D3DX12_ROOT_DESCRIPTOR_TABLE1::Initialize(RootParm.DescriptorTable, NumDescriptorRanges, DescriptorRanges);
    }
};

struct D3DX12_VERSIONED_ROOT_SIGNATURE_DESC : public D3D12_VERSIONED_ROOT_SIGNATURE_DESC
{
    D3DX12_VERSIONED_ROOT_SIGNATURE_DESC() = default;

    D3DX12_VERSIONED_ROOT_SIGNATURE_DESC(const D3D12_VERSIONED_ROOT_SIGNATURE_DESC& o) noexcept
        : D3D12_VERSIONED_ROOT_SIGNATURE_DESC(o)
    {}

    inline void Initialize_1_1(
        UINT NumParameters,
        _In_reads_opt_(NumParameters) const D3D12_ROOT_PARAMETER1* Parameters,
        UINT NumStaticSamplers = 0,
        _In_reads_opt_(NumStaticSamplers) const D3D12_STATIC_SAMPLER_DESC* StaticSamplers = nullptr,
        D3D12_ROOT_SIGNATURE_FLAGS Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE) noexcept
    {
        Initialize_1_1(*this, NumParameters, Parameters, NumStaticSamplers, StaticSamplers, Flags);
    }

    static inline void Initialize_1_1(
        _Out_ D3D12_VERSIONED_ROOT_SIGNATURE_DESC& Desc,
        UINT NumParameters,
        _In_reads_opt_(NumParameters) const D3D12_ROOT_PARAMETER1* Parameters,
        UINT NumStaticSamplers = 0,
        _In_reads_opt_(NumStaticSamplers) const D3D12_STATIC_SAMPLER_DESC* StaticSamplers = nullptr,
        D3D12_ROOT_SIGNATURE_FLAGS Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE) noexcept
    {
        Desc.Version = D3D_ROOT_SIGNATURE_VERSION_1_1;
        Desc.Desc_1_1.NumParameters = NumParameters;
        Desc.Desc_1_1.pParameters = Parameters;
        Desc.Desc_1_1.NumStaticSamplers = NumStaticSamplers;
        Desc.Desc_1_1.pStaticSamplers = StaticSamplers;
        Desc.Desc_1_1.Flags = Flags;
    }
};

// シェーダバイトコード
struct D3DX12_SHADER_BYTECODE : public D3D12_SHADER_BYTECODE
{
    D3DX12_SHADER_BYTECODE() = default;

    explicit D3DX12_SHADER_BYTECODE(const D3D12_SHADER_BYTECODE& o) noexcept :
        D3D12_SHADER_BYTECODE(o)
    {}

    // blob: バイナリ・ラージ・オブジェクト
    D3DX12_SHADER_BYTECODE(_In_ ID3DBlob* ShaderBlob) noexcept
    {
        if(ShaderBlob)
        {
            pShaderBytecode = ShaderBlob->GetBufferPointer();
            BytecodeLength = ShaderBlob->GetBufferSize();
        }
    }

    D3DX12_SHADER_BYTECODE(const void* ShaderByteCode, SIZE_T BytecodeLength) noexcept
    {
        this->pShaderBytecode = ShaderByteCode;
        this->BytecodeLength = BytecodeLength;
    }
};

// ラスタライザ設定
struct D3DX12_RASTERIZER_DESC : public D3D12_RASTERIZER_DESC
{
    D3DX12_RASTERIZER_DESC() = default;

    explicit D3DX12_RASTERIZER_DESC(D3DX12_DEFAULT) noexcept
    {
        FillMode = D3D12_FILL_MODE_SOLID;
        CullMode = D3D12_CULL_MODE_BACK;
        FrontCounterClockwise = FALSE;
        DepthBias = D3D12_DEFAULT_DEPTH_BIAS;
        DepthBiasClamp = D3D12_DEFAULT_DEPTH_BIAS_CLAMP;
        SlopeScaledDepthBias = D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS;
        DepthClipEnable = TRUE;
        MultisampleEnable = FALSE;
        AntialiasedLineEnable = FALSE;
        ForcedSampleCount = 0;
        ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;
    }
};

// ブレンド設定
struct D3DX12_BLEND_DESC : public D3D12_BLEND_DESC
{
    D3DX12_BLEND_DESC() = default;

    explicit D3DX12_BLEND_DESC(const D3D12_BLEND_DESC& o) noexcept
        :D3D12_BLEND_DESC(o)
    {}

    explicit D3DX12_BLEND_DESC(D3DX12_DEFAULT) noexcept
    {
        AlphaToCoverageEnable = FALSE;
        IndependentBlendEnable = FALSE;

        const D3D12_RENDER_TARGET_BLEND_DESC DefaultRTBlendDesc =
        {
                FALSE, FALSE, 
                D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
                D3D12_BLEND_ONE, D3D12_BLEND_ZERO, D3D12_BLEND_OP_ADD,
                D3D12_LOGIC_OP_NOOP,
                D3D12_COLOR_WRITE_ENABLE_ALL,
        };

        for(std::uint32_t i = 0; i < D3D12_SIMULTANEOUS_RENDER_TARGET_COUNT; ++i)
        {
            RenderTarget[i] = DefaultRTBlendDesc;
        }
    }
};

// ヒーププロパティ
struct D3DX12_HEAP_PROPERTIES : public D3D12_HEAP_PROPERTIES
{
    D3DX12_HEAP_PROPERTIES() = default;

    explicit D3DX12_HEAP_PROPERTIES(const D3D12_HEAP_PROPERTIES& o) noexcept :
        D3D12_HEAP_PROPERTIES(o)
    {}

    explicit D3DX12_HEAP_PROPERTIES(const D3D12_HEAP_TYPE Type, UINT CreationNodeMask = 1,
        UINT NodeMask = 1 ) noexcept
    {
        this->Type = Type;
        this->CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
        this-> MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
        this->CreationNodeMask = CreationNodeMask;
        this->VisibleNodeMask = NodeMask;
    }

    const bool IsCPUAccessible() const noexcept
    {
        return Type == D3D12_HEAP_TYPE_UPLOAD || Type == D3D12_HEAP_TYPE_READBACK || (Type == D3D12_HEAP_TYPE_CUSTOM && 
            (CPUPageProperty == D3D12_CPU_PAGE_PROPERTY_WRITE_COMBINE || CPUPageProperty == D3D12_CPU_PAGE_PROPERTY_WRITE_BACK));
    }
};

// リソース設定情報
struct D3DX12_RESOURCE_DESC : public D3D12_RESOURCE_DESC
{
    D3DX12_RESOURCE_DESC() = default;

    D3DX12_RESOURCE_DESC(
        const D3D12_RESOURCE_DIMENSION Dimension,
        const std::uint64_t Alignment,
        const std::uint64_t Width,
        const UINT Height,
        const std::uint16_t DepthOrArraySize,
        const std::uint16_t MipLevels,
        const DXGI_FORMAT Format,
        const std::uint32_t SampleCount,
        const std::uint32_t SampleQuality,
        D3D12_TEXTURE_LAYOUT Layout,
        D3D12_RESOURCE_FLAGS Flags) noexcept
    {
        this->Dimension = Dimension;
        this->Alignment = Alignment;
        this->Width = Width;
        this->Height = Height;
        this->DepthOrArraySize = DepthOrArraySize;
        this->MipLevels = MipLevels;
        this->Format = Format;
        this->SampleDesc.Count = SampleCount;
        this->SampleDesc.Quality = SampleQuality;
        this->Layout = Layout;
        this->Flags = Flags;
    }

    static inline D3DX12_RESOURCE_DESC Buffer(
        const std::uint64_t Width,
        const D3D12_RESOURCE_FLAGS Flags = D3D12_RESOURCE_FLAG_NONE,
        const std::uint64_t Alignment = 0) noexcept
    {
        return D3DX12_RESOURCE_DESC(D3D12_RESOURCE_DIMENSION_BUFFER, Alignment, Width, 1, 1, 1,
            DXGI_FORMAT_UNKNOWN, 1, 0, D3D12_TEXTURE_LAYOUT_ROW_MAJOR, Flags);
    }
};

// 範囲
struct D3DX12_RANGE : public D3D12_RANGE
{
    D3DX12_RANGE() = default;

    D3DX12_RANGE(const D3D12_RANGE& o) noexcept
        : D3D12_RANGE(o)
    {}

    D3DX12_RANGE(
        SIZE_T Begin,
        SIZE_T End
    ) noexcept
    {
        this->Begin = Begin;
        this->End = End;
    }
};

// リソースバリアー
struct D3DX12_RESOURCE_BARRIER : public D3D12_RESOURCE_BARRIER
{
    D3DX12_RESOURCE_BARRIER() = default;

    explicit D3DX12_RESOURCE_BARRIER(const D3D12_RESOURCE_BARRIER& o) noexcept
        : D3D12_RESOURCE_BARRIER(o)
    {}

    static __forceinline D3DX12_RESOURCE_BARRIER Transition(
        _In_ ID3D12Resource* Rsource,
        D3D12_RESOURCE_STATES StateBefore,
        D3D12_RESOURCE_STATES StateAfter,
        std::uint32_t SubResource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES,
        D3D12_RESOURCE_BARRIER_FLAGS Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE) noexcept
    {
        D3DX12_RESOURCE_BARRIER Result = {};
        D3D12_RESOURCE_BARRIER& Barrier = Result;
        Result.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
        Result.Flags = Flags;
        Barrier.Flags = Flags;
        Barrier.Transition.pResource = Rsource;
        Barrier.Transition.StateBefore = StateBefore;
        Barrier.Transition.StateAfter = StateAfter;
        Barrier.Transition.Subresource = SubResource;

        return Result;
    }

};

// ルートシグネチャのヘルパー関数
inline HRESULT D3DX12SerializeVersionedRootSignature(
    _In_ const D3D12_VERSIONED_ROOT_SIGNATURE_DESC* RootSignatureDesc,
    D3D_ROOT_SIGNATURE_VERSION MaxVersion,
    _Outptr_ ID3DBlob** Blob,
    _Always_(_Outptr_result_maybenull_) ID3DBlob** ErrorBlob) noexcept
{
    if(ErrorBlob != nullptr)
    {
        *ErrorBlob = nullptr;
    }

    switch(MaxVersion)
    {
    
        case D3D_ROOT_SIGNATURE_VERSION_1_1:
        {
            return D3D12SerializeVersionedRootSignature(RootSignatureDesc, Blob, ErrorBlob);
        }

        default:
        {
            OutputDebugStringEx(L"D3DX12SerializeVersionedRootSignature: Not Find. \n");
            exit(1);
        }

    }

}